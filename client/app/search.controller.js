'use strict';

(function () {
    angular
        .module("MyApp")
        .controller("SearchCtrl", MyCtrl);

    MyCtrl.$inject = ["SearchServices", "$stateParams", "$state"];

    function MyCtrl(SearchServices, $stateParams, $state) {
        var myCtrl = this;
        myCtrl.brandname = "";
        myCtrl.productname = "";

        myCtrl.searchObject = {
            name: "",
            id: "",
            brand: "",
            upc12: ""
        };

        myCtrl.products = [];

        function initData() {
            SearchServices.listproducts()
                .then(function (result) {
                    console.log("search list");
                    console.log(result);
                    myCtrl.products = result;
                })
                .catch(function (err) {
                    console.log("error");
                });
        }

        initData();

        myCtrl.searchname;
        myCtrl.searchProduct = function searchProduct() {
            if (myCtrl.brandname != "") {
                myCtrl.searchname = myCtrl.brandname;
            } else if (myCtrl.productname != "") {
                myCtrl.searchname = myCtrl.productname;
            }
            console.log("Search Product Controller: " + myCtrl.searchname);

            SearchServices.search(myCtrl.searchname)
                .then(function (results) {
                    myCtrl.products = results;
                    console.log("search product success: " + results);
                })
                .catch(function (err) {
                    console.log("search product error" + err);
                });

        };

        myCtrl.editProduct = function editProduct(myid) {
            console.log("edit product called");
            myCtrl.searchObject.name = "";

            $state.go("B", {id: myid});
            // SearchServices.editProduct(myCtrl.seachObject)
            //     .then(function (results) {
            //         console.log("edit product success: ");
            //     })
            //     .catch(function (err) {
            //         console.log("edit product error" + err);
            //     });
        }

    }
})();

