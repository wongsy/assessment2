/**
 * Created by Wong SY on 08 Nov 2016
 */

(function() {
    angular.module("MyApp", ["ui.router", "angularModalService"]);
})();