'use strict';

(function () {
    angular
        .module("MyApp")
        .controller("ProductCtrl", MyCtrl);

    MyCtrl.$inject = ["ProductServices", "$stateParams"];

    function MyCtrl(ProductServices, $stateParams) {
        var myCtrl = this;

        myCtrl.searchObject = {
            name: "",
            id: $stateParams.id,
            brand: "",
            upc12: ""
        };
        console.log("product controller " + myCtrl.id);

        myCtrl.products = [];

        myCtrl.updateProduct = function updateProduct() {
            console.log("Search Product Controller" + myCtrl.searchObject);

            ProductServices.updateProduct(myCtrl.searchObject)
                .then(function (result) {
                    myCtrl.products = result;
                })
                .catch(function (err) {
                    cosole.log("search controller error" + err);
                });

        };

    }
})();

