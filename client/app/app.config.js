'use strict';
(function () {
    angular
        .module("MyApp")
        .config(MyAppConfig);

    MyAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function MyAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl :'../views/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'vm'
            })
            .state('B', {
                url: '/B',
                templateUrl: '../views/product.html',
                controller: 'ProductCtrl',
                controllerAs: 'vm'
            })

        $urlRouterProvider.otherwise("/A");
    }
})();