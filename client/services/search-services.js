'use strict';

(function () {
    angular
        .module("MyApp")
        .service("SearchServices", SearchServices);

    SearchServices.$inject = ["$http", "$q"];

    console.log("Entered Search Services");
    function SearchServices($http, $q) {
        var obj = {};

        obj.listproducts = function() {
            var deferred = $q.defer();
            $http
                .get("/api/search")
                .then(function (response) {
                    console.log("listprodcts-services log ");
                    deferred.resolve(response.data);
                }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        obj.search = function(searchname) {
            console.log("server search called");
            var deferred = $q.defer();
            $http
                .get("/api/search/" + searchname)
                .then(function (response) {
                    console.log("search-services log ");
                    deferred.resolve(response.data);
                }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        obj.editProduct = function(product) {
            console.log("server search called");
            var deferred = $q.defer();
            $http
                .update("/api/update" + product)
                .then(function (response) {
                    console.log("search-services log ");
                    deferred.resolve(response.data);
                }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        return obj;
    }
})();

