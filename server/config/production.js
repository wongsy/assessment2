
'use strict';

module.exports = {
    mysql: {
        host:     'localhost',
        username: 'root',
        password: 'myrootpwd',
        database: 'grocerydb'
    }
};