/**
 * Created by Wong SY on 08 Nov 2016.
 */
'use strict';

const express = require("express");

const path = require("path");
const docroot = path.join(__dirname, "..");
const CLIENT_FOLDER = path.join(docroot, "client");
const BOWER_FOLDER = path.join(docroot, "bower_components");

const SearchController = require("./api/search/search.controller");
const UpdateController = require("./api/search/updateproduct.controller");

// const UserController = require("./api/user/user.controller");
// const PhotoController = require("./api/photo/photo.controller");
// const AWSController = require("./api/aws/aws.controller");

module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
};

function configureRoutes(app){
    app.get("/api/search/:name", SearchController.getProduct);
    app.get("/api/search", SearchController.search);
    app.put("/api/product", UpdateController.updateProduct);
    
    // app.post("/api/product", UpdateController.addProduct);


    app.use(express.static(CLIENT_FOLDER));
    app.use("/bower_components",express.static(BOWER_FOLDER));
//    console.log(CLIENT_FOLDER);
//    console.log(BOWER_FOLDER);
}

function errorHandler(app) {
    app.use(function (req, res) {
        console.log("Client Error 400", req.headers);
        //res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        console.log("Server Error 500",err);
        //res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
}


