/**
 * Created by Wong SY on 8 Nov 2016.
 */
var Sequelize = require("sequelize");

module.exports = function (database) {
    var GroceryModel =
        database.define("grocery_list", {
                id: {type: Sequelize.INTEGER, primaryKey: true},
                upc12: {type: Sequelize.INTEGER},
                brand: {type: Sequelize.STRING},
                name: {type: Sequelize.STRING}
            }
            , {
                tableName: 'grocery_list',
                timestamps: false
            }
        );

    return GroceryModel;
};
