/*
  File: database.js 

  Define:
   1. Database Connection
   2. The Data Models - Database Table
 */

var Sequelize = require('sequelize');
var config = require('./config');

//conn equiv now database
var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password, {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var GroceryModel = require('./model/grocery.model')(database);

module.exports = {
    GroceryModel: GroceryModel
};

