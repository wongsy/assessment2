'use strict';

var SearchModel = require('../../database').GroceryModel;

exports.updateProduct = function (req, res) {
    var where = {};
    where.id=req.body.id
    SearchModel.update(
        {
            name: req.body.name,
            brand: req.body.brand,
            upc12: req.body.upc12
        },
    {where: where})

        .then(function (results) {
            console.log(results);
            res.status(200).send("Record updated");
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send("Failed to update record");

        });
};

// exports.search = function (req, res) {
//     GroceryModel.findAll()
//         .then(function (response) {
//             console.log("database search called");
//             res.json(response);
//         }).catch(function (error) {
//         res.status(500).send({'error': error});
//     });
// };




