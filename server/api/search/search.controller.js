'use strict';

var GroceryModel = require('../../database').GroceryModel;

exports.search = function (req, res) {
    // var where = {};
    // where.name = {$like: "%" + req.params.name + "%"};
    // console.log("server - search called");
    GroceryModel.findAll(
        {limit: 20}
    )
        .then(function (response) {
            console.log("database search successful");
            res.json(response);
        }).catch(function (error) {
        console.log("database search error" + error);
        res.status(500).send({'error': error});
    });
};
exports.getProduct = function (req, res) {
    console.log("server search controller called. req: " + req.params.name);
    var where = {};
    //where.name = req.query.name;
    //where.name = {$like: "%" + req.query.name + "%"};
    //where.name = {$like: "%" + req.params.name + "%"};


    console.log("Before getGrocery value: " + JSON.stringify(req.query));

    // GroceryModel.findAll({
    //     where : where
    // })
    GroceryModel.findAll({
        where : {name: {$like: "%" + req.params.name + "%"}}
    //     where: (
    //         {name: {$like: "%" + req.params.name + "%"}} and
    //         {brand: {$like: "%" + req.params.name + "%"}})
     })

        .then(function (response) {
            console.log("getGrocery value: " + req.query.name);
            res.json(response);
        })
        .catch(function (error) {
            res.status(500).send({'error': error});
        });
}




