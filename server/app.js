/**
 * Created by Wong SY on 8 Nov 2016
 */
const express = require("express");
const app = express();

// parse body of req
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(bodyParser.json()); // for parsing application/json

const routes = require('./routes');
routes.init(app);
routes.errorHandler(app);

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function () {
    console.log("Server started on port %d", app.get("port"));
});

